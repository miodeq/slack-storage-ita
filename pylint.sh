#!/bin/sh

pylint --rcfile=.pylintrc -f colorized --ignore=apps/core/migrations --load-plugins pylint_django --load-plugins pylint_django.checkers.db_performance apps