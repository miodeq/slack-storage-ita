#!/bin/sh

echo "${0}: [1] Generating database dump..."
python manage.py dumpdata --exclude auth.permission --exclude contenttypes | gzip > media/slack_db.json.gz