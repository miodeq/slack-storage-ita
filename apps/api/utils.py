# -*- coding: utf-8 -*-

from datetime import datetime

import pytz


def convert_message_timestamp(value):
    """
    Converts UTC timestamp into datetime object.
    """

    tz = pytz.timezone("UTC")

    if isinstance(value, int):
        return tz.localize(datetime.utcfromtimestamp(value))

    elif isinstance(value, str):
        value = value.split(".")
        return tz.localize(datetime.utcfromtimestamp(int(value[0])))
