from slack import WebClient

from apps.core.utils import init_logger

logger = init_logger(name=f"[Slack API Collector: {__name__}]")


class SlackCollector:
    """
        Slack collector
        Scritpt to gather data using slack_storage API

        Necessary scopes and description
        channels:history
            View messages and other content in the user’s public channels
        files:read
            View files shared in channels and conversations that the user has access to
        identify
            View information about the user’s identity
        users:read
            View people in the workspace
    """

    def __init__(self, token):
        self.token = token

        self.client = self.set_client()

        self.team_info = dict()
        self.channels_info = list()
        self.users_info = list()
        self.messages_info = list()
        self.files_info = list()

    def set_client(self):
        """
        Returns Slack API conn instance.
        """

        return WebClient(token=self.token)

    def get_workspace_info(self):
        """
        Method to extract data from team_info API call.
        """

        workspace = self.client.team_info().get("team")

        self.team_info = {
            "team_id": workspace.get("id"),
            "name": workspace.get("name"),
            "domain": workspace.get("domain"),
            "domain_email": workspace.get("email_domain"),
            "icon": workspace.get("icon").get("image_original"),
        }

    def get_channels_info(self):
        """
        Method to extract data from channels_list API call.
        """

        self.channels_info.extend(
            {
                "channel_id": channel.get("id"),
                "name": channel.get("name"),
                "created_date": channel.get("created"),
                "creator": channel.get("creator"),
                "topic": channel.get("topic").get("value"),
                "purpose": channel.get("purpose").get("value"),
                "num_members": channel.get("num_members"),
            }
            for channel in self.client.channels_list().get("channels")
        )

    def get_users_info(self):
        """
        Method to extract data from users_list API call.
        """

        self.users_info.extend(
            {
                "user_id": member.get("id"),
                "team": member.get("team_id"),
                "name": member.get("real_name"),
                "avatar": member.get("profile").get("image_192"),
                "phone": member.get("profile").get("phone"),
                "skype": member.get("profile").get("skype"),
                "is_admin": member.get("is_admin"),
                "is_owner": member.get("is_owner"),
                "is_bot": member.get("is_bot"),
            }
            for member in self.client.users_list().get("members")
        )

    def get_messages_info(self):
        """
        Method to extract data from channel_history API call.
        """

        channels_id = [
            channel_id.get("channel_id") for channel_id in self.channels_info
        ]

        for channel_id in channels_id:
            oldest = "0000000001.000000"

            while oldest:
                channel_history = self.client.channels_history(
                    channel=channel_id, count=1000, oldest=oldest
                ).get("messages")

                for msg in channel_history[::-1]:
                    self.messages_info.append(
                        {
                            "channel": channel_id,
                            "user": msg.get("user"),
                            "team": msg.get("team"),
                            "msg_type": msg.get("type"),
                            "msg": msg.get("text"),
                            "timestamp": msg.get("ts"),
                            "msg_id": msg.get("client_msg_id"),
                            "is_modified": True if msg.get("edited") else False,
                            "timestamp_modified": msg.get("edited").get("ts")
                            if msg.get("edited")
                            else [],
                        }
                    )

                oldest = channel_history[0].get("ts") if channel_history else ""

    def get_files_info(self):
        """
        Method to collect user files from workspace
        """

        pages_count = self.client.files_list(count=200).get("paging").get("pages")

        for page in range(1, pages_count + 1):

            files_list = self.client.files_list(count=200, page=page).get("files")

            for _file in files_list:
                self.files_info.append(
                    {
                        "file_id": _file.get("id"),
                        "user": _file.get("user"),
                        "channel": _file.get("channels")[0]
                        if len(_file.get("channels")) == 1
                        else None,
                        "timestamp": _file.get("timestamp"),
                        "file_type": _file.get("filetype"),
                        "permalink": _file.get("url_private_download"),
                    }
                )

    def run(self):
        """
        main pipeline
        """

        # order of envoking methods
        self.get_workspace_info()
        self.get_channels_info()
        self.get_users_info()
        self.get_messages_info()
        self.get_files_info()

    def get_team_data(self):
        return self.team_info

    def get_channels_data(self):
        return self.channels_info

    def get_users_data(self):
        return self.users_info

    def get_messages_data(self):
        return self.messages_info

    def get_files_data(self):
        return self.files_info
