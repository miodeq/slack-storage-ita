# -*- coding: utf-8 -*-

import os

import requests
from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.files.base import ContentFile
from django.http import JsonResponse
from slack.errors import SlackClientError
from user_agent import generate_user_agent

from apps.api.collector import SlackCollector
from apps.api.utils import convert_message_timestamp

logger = get_task_logger(__name__)


@shared_task(
    name="make_connection",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def make_connection(self, token, users=False, messages=False, files=False):
    """
    Task to make connection to Slack Api
    :param self: required to make retries
    :param token: Slack API token
    :param users: flag to collect user info
    :param messages: flag to collect messages info
    :param files: flag to collect files info
    :return: SlackCollector instance
    """
    while 1:
        try:
            logger.info("Initialising connection with Slack API...")

            instance = SlackCollector(token=token)

            logger.info("Successfully connected to Slack API.")
            logger.info("Retrieving data from Slack API...")

            instance.get_channels_info()

            if users:
                instance.get_workspace_info()
                instance.get_users_info()

            elif messages:
                instance.get_messages_info()

            elif files:
                instance.get_files_info()

            logger.info("Preparing data to be added to DB...")
        except SlackClientError as exc:
            logger.error(f"Cannot connect to Slack API --> {exc}")
            logger.error(
                f"Sleeping for {2 ** self.request.retries} before making another try..."
            )

            self.retry(exc=exc, countdown=2 ** self.request.retries)
        else:
            break

    return instance


@shared_task(
    name="collect_team_users",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def collect_team_users(self):
    """
    Celery task for collecting data from API.
    """

    from apps.core.models import Team, User, Channel
    from apps.core.models import TokenSingleton

    logger.info("Starting celery task to get data from Slack API...")

    slack_config = None

    try:
        slack_config = TokenSingleton.objects.get()
        task_active = slack_config.task_active
    except TokenSingleton.DoesNotExist:
        task_active = False

    if task_active:
        instance = make_connection(slack_config.token, users=True)

        try:
            logger.info("Inserting data to database...")
            # insert team info data
            team, _ = Team.objects.get_or_create(instance.get_team_data())

            # insert users data
            logger.info("Inserting user data...")

            for user_item in instance.get_users_data():
                _, _ = User.objects.get_or_create(
                    **{
                        "user_id": user_item.get("user_id"),
                        "team": team,
                        "name": user_item.get("name"),
                        "avatar": user_item.get("avatar"),
                        "phone": user_item.get("phone"),
                        "skype": user_item.get("skype"),
                        "is_admin": user_item.get("is_admin"),
                        "is_owner": user_item.get("is_owner"),
                        "is_bot": user_item.get("is_bot"),
                    }
                )

            logger.info("Added user data.")

            # insert channels data
            logger.info("Inserting channels data...")

            for channel_item in instance.get_channels_data():
                _creator = User.objects.filter(user_id=channel_item.get("creator"))

                if _creator.exists():
                    _, _ = Channel.objects.get_or_create(
                        **{
                            "name": channel_item.get("name"),
                            "channel_id": channel_item.get("channel_id"),
                            "created_date": convert_message_timestamp(
                                channel_item.get("created_date")
                            ),
                            "creator": _creator[0],
                            "topic": channel_item.get("topic"),
                            "purpose": channel_item.get("purpose"),
                            "num_members": channel_item.get("num_members"),
                        }
                    )

            logger.info("Added channels data.")

            return JsonResponse(
                data={"msg": "Successfully added new data to database."}, status=200
            )

        except Exception as exc:
            logger.exception(f"Exception --> {exc}")
            self.retry(exc=exc, countdown=2 ** self.request.retries)


# insert messages data
@shared_task(
    name="collect_messages_data",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def collect_messages_data(self):
    """
    Celery task for collecting data from API.
    """

    from apps.core.models import Team, User, Channel
    from apps.core.models import Message, TokenSingleton
    from django.conf import settings

    import json
    import re

    logger.info("Starting celery task to get data from Slack API...")

    slack_config = None

    try:
        slack_config = TokenSingleton.objects.get()
        task_active = slack_config.task_active
    except TokenSingleton.DoesNotExist:
        task_active = False

    if task_active:
        instance = make_connection(slack_config.token, messages=True)

        try:
            logger.info("Inserting data to database...")

            for msg_item in instance.get_messages_data():
                _channel = Channel.objects.filter(channel_id=msg_item.get("channel"))
                _user = User.objects.filter(user_id=msg_item.get("user"))
                team, _ = Team.objects.get_or_create(instance.get_team_data())
                is_modified = msg_item.get("is_modified", False)
                timestamp = convert_message_timestamp(msg_item.get("timestamp"))
                timestamp_modified = convert_message_timestamp(
                    msg_item.get("timestamp_modified", msg_item.get("timestamp"))
                )

                _msg = Message.objects.filter(timestamp=timestamp).first()
                if _channel.exists() and _user.exists():
                    if is_modified and _msg:
                        if (
                                _msg.timestamp_modified
                                and timestamp_modified > _msg.timestamp_modified
                        ):
                            _msg.is_modified = is_modified
                            _msg.msg_modified = msg_item.get("msg", [])
                            _msg.timestamp_modified = timestamp_modified
                            _msg.save()

                    else:
                        news = msg_item.get("msg")

                        try:
                            lis = re.search(":(.*):", news)
                            if lis:
                                items = set(lis.group().split(":"))
                                items.remove("")
                                url = os.path.join(
                                    settings.BASE_DIR,
                                    "static/template_static/json/emojis.json",
                                )
                                with open(url, "r") as plik:
                                    _json = json.load(plik)
                                tmp = None
                                for emoji in _json:
                                    for item in items:
                                        if (
                                                emoji.get("description") == item
                                                or item in emoji.get("aliases")
                                                or item.replace("_", " ")
                                                == emoji.get("description")
                                        ):
                                            news = news.replace(
                                                ":" + item + ":", emoji.get("emoji")
                                            )
                                            tmp = item
                                    if tmp != None:
                                        items.remove(tmp)
                                        tmp = None
                        except Exception as exc:
                            logger.exception(f"Error with Emoji: {exc}")

                        _, _ = Message.objects.get_or_create(
                            **{
                                "channel": _channel.first(),
                                "user": _user.first(),
                                "team": team,
                                "msg_type": msg_item.get("msg_type"),
                                "msg": news,
                                "timestamp": convert_message_timestamp(
                                    msg_item.get("timestamp")
                                ),
                                "msg_id": msg_item.get("msg_id"),
                                "is_modified": msg_item.get("is_modified", False),
                                "msg_modified": msg_item.get("msg", []),
                                "timestamp_modified": convert_message_timestamp(
                                    msg_item.get(
                                        "timestamp_modified", msg_item.get("timestamp")
                                    )
                                ),
                            }
                        )

            logger.info("Added messages data.")
            return JsonResponse(
                data={"msg": "Successfully added new data to database."}, status=200
            )

        except Exception as exc:
            logger.exception(f"Exception --> {exc}")
            self.retry(exc=exc, countdown=2 ** self.request.retries)


# insert files data
@shared_task(
    name="collect_files_data",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def collect_files_data(self):
    """
    Celery task for collecting data from API.
    """

    from apps.core.models import User, Channel
    from apps.core.models import File, TokenSingleton

    logger.info("Starting celery task to get data from Slack API...")

    slack_config = None

    try:
        slack_config = TokenSingleton.objects.get()
        task_active = slack_config.task_active
    except TokenSingleton.DoesNotExist:
        task_active = False

    if task_active:
        instance = make_connection(slack_config.token, files=True)

        try:
            logger.info("Inserting user files data...")

            for file_item in instance.get_files_data():
                _user = User.objects.filter(user_id=file_item.get("user"))
                _channel = Channel.objects.filter(channel_id=file_item.get("channel"))

                if _user.exists() and _channel.exists():
                    _, _ = File.objects.get_or_create(
                        **{
                            "file_id": file_item.get("file_id"),
                            "user": _user[0],
                            "channel": _channel[0],
                            "timestamp": convert_message_timestamp(
                                file_item.get("timestamp")
                            ),
                            "file_type": file_item.get("file_type"),
                            "permalink": file_item.get("permalink"),
                        }
                    )

            logger.info("Added files data...")

            return JsonResponse(
                data={"msg": "Successfully added new data to database."}, status=200
            )

        except Exception as exc:
            logger.exception(f"Exception --> {exc}")
            self.retry(exc=exc, countdown=2 ** self.request.retries)


@shared_task(
    name="collect_single_user_avatar",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def collect_single_user_avatar(self, pk):
    """
    Celery task to collect single user's avatar file.
    """

    from apps.core.models import User

    headers = {
        "User-Agent": generate_user_agent(),
    }

    try:
        instance = User.objects.get(pk=pk)

        instance_url = instance.avatar

        logger.info(f"Downloading --> {instance_url}")

        instance_name = os.path.basename(instance_url)

        response = requests.get(
            url=instance_url, headers=headers, timeout=10, stream=True
        )

        logger.info(f"Response --> {response.status_code}")

        if response.status_code != 200:
            return JsonResponse(
                data={
                    "msg": "Could'nt download avatar file.",
                    "error": response.text.strip(),
                },
                status=response.status_code,
            )

        instance.local_avatar.save(
            instance_name, ContentFile(response.content), save=True
        )

        instance.is_avatar_downloaded = True
        instance.save()

        logger.info(f"Successfully downloaded Avatar for user PK={pk}")

        avatar_path = os.path.join(instance.local_avatar.path)
        os.chmod(avatar_path, 0o777)

        return JsonResponse(data={"msg": "Successfully added avatars."}, status=200)

    except Exception as exc:
        logger.exception(f"Exception --> {exc}")
        self.retry(exc=exc, countdown=2 ** self.request.retries)


@shared_task(
    name="collect_user_avatars", queue="slack_api_collector", ignore_result=True
)
def collect_user_avatars():
    """
    Celery task for collecting user avatars from API.
    """

    from apps.core.models import User

    users_pk = User.objects.filter(is_avatar_downloaded=False).values_list(
        "pk", flat=True
    )

    for user_pk in users_pk:
        collect_single_user_avatar.delay(user_pk)


# user files
@shared_task(
    name="collect_single_user_file",
    queue="slack_api_collector",
    max_retries=10,
    bind=True,
    ignore_result=True,
)
def collect_single_user_file(self, pk):
    """
    Celery task to collect single user's uploaded file.
    """

    from apps.core.models import File, TokenSingleton

    api_token = TokenSingleton.get_solo().token

    headers = {
        "Authorization": "Bearer " + api_token,
        "User-Agent": generate_user_agent(),
    }

    try:
        instance = File.objects.get(pk=pk)

        instance_url = instance.permalink

        logger.info(f"Downloading --> {instance_url}")

        img_ext = ["jpg", "gif", "png"]

        _path, _type = os.path.basename(instance_url), instance.file_type

        instance_name = (
            f"{_path}.{_type}"
            if instance.user.is_bot & any(_type == ext for ext in img_ext)
            else _path
        )

        response = requests.get(
            url=instance_url, headers=headers, timeout=10, stream=True
        )

        logger.info(f"Response --> {response.status_code}")

        if response.status_code != 200:
            return JsonResponse(
                data={
                    "msg": "Could'nt download local file.",
                    "error": response.text.strip(),
                },
                status=response.status_code,
            )

        instance.local_file.save(
            instance_name, ContentFile(response.content), save=True
        )

        instance.is_file_downloaded = True
        instance.save()

        logger.info(f"Successfully downloaded File for PK={pk}")

        file_path = os.path.join(instance.local_file.path)
        os.chmod(file_path, 0o777)

        return JsonResponse(data={"msg": "Successfully downloaded file."}, status=200)

    except Exception as exc:
        logger.exception(f"Exception --> {exc}")
        self.retry(exc=exc, countdown=2 ** self.request.retries)


@shared_task(name="collect_user_files", queue="slack_api_collector", ignore_result=True)
def collect_user_files():
    """
    Celery task for collecting user files from API.
    """

    from apps.core.models import File

    files_pk = File.objects.filter(is_file_downloaded=False).values_list(
        "pk", flat=True
    )

    for file_pk in files_pk:
        collect_single_user_file.delay(file_pk)
