import os

from django.contrib import admin
from django.utils.safestring import mark_safe

from apps.core.models import TokenSingleton, Message, Team
from apps.core.models import User, Channel, File


@admin.register(TokenSingleton)
class TokenSingletonAdmin(admin.ModelAdmin):
    """
    TokenSingleton admin interface.
    """

    fieldsets = [("Page configuration", {"fields": ["token", "task_active", ]})]


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    """
    Team admin interface.
    """

    list_display = ("id", "name", "domain", "team_id", "domain_email", "icon")

    list_per_page = 10

    list_filter = ("domain", "domain_email")

    search_fields = ("name", "domain", "team_id", "domain_email")

    readonly_fields = ("icon", "domain_email", "domain", "name", "team_id")


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    """
    User admin interface.
    """

    list_display = (
        "id",
        "name",
        "team",
        "user_id",
        "phone",
        "skype",
        "get_avatar_as_thumbnail",
    )

    search_fields = ("name", "team__name", "user_id", "phone", "skype")

    readonly_fields = (
        "is_bot",
        "is_admin",
        "is_owner",
        "user_id",
        "phone",
        "skype",
        "is_avatar_downloaded",
        "name",
    )

    list_filter = ("is_bot", "is_admin", "is_owner", "team", "is_avatar_downloaded")

    raw_id_fields = ("team",)

    def get_avatar_as_thumbnail(self, obj):
        """
        Returns user avatar as thumbnail.
        """

        if obj.local_avatar:
            _url = obj.local_avatar.path.split("src")[-1]
            return mark_safe(
                f'<img class="img-responsive" src="{_url}" width="60" height="60" />'
            )
        return "No avatar found"

    get_avatar_as_thumbnail.allow_tags = True
    get_avatar_as_thumbnail.short_description = "Avatar"


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    """
    Channel admin interface.
    """

    list_display = (
        "id",
        "name",
        "channel_id",
        "created_date",
        "creator",
        "topic",
        "purpose",
        "num_members",
    )

    raw_id_fields = ("creator",)

    search_fields = ("creator__name", "creator__user_id")

    list_filter = ("created_date", "creator__name")

    readonly_fields = ("channel_id", "num_members", "name", "topic", "purpose")

    ordering = ('-num_members', )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """
    Message admin interface.
    """

    list_display = ("id", "user", "channel", "msg", "timestamp")

    search_fields = ("msg", "channel__name", "user__name")

    raw_id_fields = ("channel", "user", "team")

    list_per_page = 50

    list_filter = ("channel__name", "timestamp", "msg_type", "team")

    readonly_fields = ("msg_type", "msg_id", "msg")


@admin.register(File)
class FileAdmin(admin.ModelAdmin):
    """
    File admin interface.
    """

    list_display = ("id", "channel", "user", "timestamp", "file_type", "get_local_file")

    raw_id_fields = ("user", "channel")

    list_filter = ("channel", "is_file_downloaded", "file_type")

    search_fields = ("file_id", "user__name", "channel__name", "permalink", "file_type")

    readonly_fields = ("permalink", "file_type", "is_file_downloaded", "file_id")

    list_per_page = 30

    def get_local_file(self, obj):
        """
        Returns proper path to the local file.
        """

        if not obj.local_file:
            return mark_safe('<span style="color:red">No file...</span>')

        _url = obj.local_file.url.split("media")[-1]

        filename = os.path.basename(obj.local_file.path)

        return mark_safe(f'<a href="/media{_url}" target="_blank">{filename}</a>')

    get_local_file.allow_tags = True
    get_local_file.short_description = "Local file"
    get_local_file.admin_order_field = "local_file"
