# -*- coding: utf-8 -*-

import logging
import os
import sys
import time
import uuid

from django.conf import settings


def init_logger(name=None, level=None):
    """
    Basic logger with timestamp.
    """

    logger = logging.getLogger(name=name or "[Django Slack Storage]")

    log_format = "%(asctime)-15s %(levelname)s %(name)s %(message)s"

    formatter = logging.Formatter(log_format)

    handler_stream = logging.StreamHandler(sys.stdout)
    handler_stream.setFormatter(formatter)

    logger.addHandler(handler_stream)

    logger.setLevel(level=level or logging.INFO)

    return logger


def prepare_directories(name):
    """
    Creates dirs and subdirs for given fileupload to avoid lack of space in storage.
    """

    data_dirs_datetime = str(time.strftime("%y/%m/%d/%H/%M"))

    file_path = os.path.join(f"{settings.MEDIA_ROOT}/{name}/{data_dirs_datetime}")

    if not os.path.exists(file_path):
        os.makedirs(file_path)

    return file_path


def multimedia_path(instance, filename):
    """
    Upload path for uploaded files.
    """

    file_path = prepare_directories("files")

    _, ext = os.path.splitext(filename)

    file_name = str(uuid.uuid4())

    _path = f"{file_path}/{file_name}{ext}"

    return os.path.join(_path)


def avatar_path(instance, filename):
    """
    Upload path for uploaded files.
    """

    file_path = prepare_directories("avatar")

    _, ext = os.path.splitext(filename)

    file_name = str(uuid.uuid4())

    _path = f"{file_path}/{file_name}{ext}"

    return os.path.join(_path)
