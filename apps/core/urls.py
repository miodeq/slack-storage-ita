from django.urls import path

from apps.core.views import HomeView, FilesView, UsersView
from apps.core.views import MessagesView, SearchView, UserProfileView

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("files/", FilesView.as_view(), name="files"),
    path("users/", UsersView.as_view(), name="users"),
    path("channel/<channel_name>/", MessagesView.as_view(), name="messages"),
    path("search/", SearchView.as_view(), name="search"),
    path("users/<int:pk>/", UserProfileView.as_view(), name="user_profile"),
]
