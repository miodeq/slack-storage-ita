from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from solo.models import SingletonModel

from apps.core.utils import multimedia_path, avatar_path


class TokenSingleton(SingletonModel):
    """
    Token model.
    """

    token = models.CharField(
        max_length=100, verbose_name="API Token", blank=False, null=False
    )

    task_active = models.BooleanField(
        verbose_name="Run periodic tasks?", default=False, blank=False, null=False
    )

    def __str__(self):
        """
        String representation of model object.
        """

        return self.token


class Team(models.Model):
    """
    Team model.
    """

    team_id = models.CharField(
        verbose_name="Slack ID", db_index=True, null=True, blank=True, max_length=20
    )

    name = models.CharField(
        verbose_name="Workspace", null=True, blank=True, max_length=255
    )

    domain = models.CharField(
        verbose_name="Domain", db_index=True, null=True, max_length=255
    )

    domain_email = models.TextField(
        verbose_name="Domain E-mails",
        null=True,
        blank=True,
        # max_length=255
    )

    icon = models.URLField(verbose_name="Icon", blank=True, null=True)

    def __str__(self):
        """
        String representation of model object.
        """

        return self.name

    class Meta:
        verbose_name = "Team"
        verbose_name_plural = "Teams"


class User(models.Model):
    """
    User model.
    """

    user_id = models.CharField(
        verbose_name="Slack ID", blank=True, null=True, max_length=255
    )

    team = models.ForeignKey(
        verbose_name="Team", to="Team", blank=True, null=True, on_delete=models.CASCADE
    )

    name = models.CharField(
        verbose_name="Username", blank=True, null=True, db_index=True, max_length=255
    )

    avatar = models.URLField(verbose_name="Avatar", blank=True, null=True)

    local_avatar = models.FileField(
        verbose_name="Local Avatar", upload_to=avatar_path, blank=True, null=True,
    )

    is_avatar_downloaded = models.BooleanField(
        verbose_name="Is avatar downloaded?", default=False, db_index=True
    )

    phone = models.CharField(verbose_name="Phone", blank=True, null=True, max_length=25)

    skype = models.CharField(verbose_name="Skype", blank=True, null=True, max_length=50)

    is_admin = models.BooleanField(
        verbose_name="Admin", default=False, blank=True, null=True, db_index=True
    )

    is_owner = models.BooleanField(
        verbose_name="Owner", default=False, blank=True, null=True, db_index=True
    )

    is_bot = models.BooleanField(verbose_name="BOT Check", default=False, db_index=True)

    @property
    def get_local_avatar(self):
        """
        Returns proper path to avatar file.
        """

        return (
            self.local_avatar.path.split("src")[-1]
            if self.local_avatar
            else self.avatar
        )

    def __str__(self):
        """
        String representation of model object.
        """

        return self.user_id if self.user_id is not None else ''

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class Channel(models.Model):
    """
    Channel model.
    """

    name = models.CharField(
        verbose_name="Channel name",
        db_index=True,
        blank=True,
        null=True,
        max_length=255,
    )

    channel_id = models.CharField(
        verbose_name="Slack ID", blank=True, null=True, max_length=255
    )

    created_date = models.DateTimeField(
        verbose_name="Created", blank=True, null=True, editable=False
    )

    creator = models.ForeignKey(
        verbose_name="Creator",
        to="User",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    topic = models.CharField(
        verbose_name="Topic", null=True, blank=True, max_length=255
    )

    purpose = models.CharField(
        verbose_name="Purpose", null=True, blank=True, max_length=255
    )

    num_members = models.PositiveSmallIntegerField(
        verbose_name="Members", default=0, db_index=True
    )

    def __str__(self):
        """
        String representation of model object.
        """

        return self.name

    class Meta:
        verbose_name = "Channel"
        verbose_name_plural = "Channels"


class Message(models.Model):
    """
    Message admin.
    """

    channel = models.ForeignKey(
        verbose_name="Channel",
        to="Channel",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        verbose_name="User", to="User", blank=True, null=True, on_delete=models.CASCADE
    )

    team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.CASCADE)

    msg_type = models.CharField(
        verbose_name="Type", null=True, blank=True, max_length=50
    )

    msg = models.TextField(verbose_name="Message", blank=True, null=True)

    timestamp = models.DateTimeField(
        verbose_name="Timestamp", blank=True, null=True, editable=False
    )

    msg_id = models.CharField(
        verbose_name="Slack ID", blank=True, null=True, max_length=50
    )

    is_modified = models.BooleanField(
        verbose_name="Is message modified?", default=False, db_index=True
    )

    msg_modified = models.TextField(
        verbose_name="Edited Message", blank=True, null=True
    )

    timestamp_modified = models.DateTimeField(
        verbose_name="Timestamp", blank=True, null=True, editable=False
    )

    @property
    def get_username(self):
        """
        Returns real user name.
        """

        return self.user.name

    @property
    def get_msg(self):
        """
        Returns message.
        """

        if self.is_modified:
            return self.msg_modified
        return self.msg

    @property
    def get_timestamp(self):
        """
        Returns timestamp
        """
        if self.is_modified:
            return self.timestamp_modified
        return self.timestamp

    def __repr__(self):
        """
        Object representation.
        """

        return f"<{self.channel.name}>: <{self.msg}>"

    def __str__(self):
        """
        String representation of model object.
        """

        return self.msg_id if self.msg_id is not None else str(self.pk)

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"


class File(models.Model):
    """
    File model.
    """

    file_id = models.CharField(
        verbose_name="Slack ID", blank=True, null=True, max_length=255
    )

    user = models.ForeignKey(
        verbose_name="User", to="User", blank=True, null=True, on_delete=models.CASCADE
    )

    channel = models.ForeignKey(
        verbose_name="Channel",
        to="Channel",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    timestamp = models.DateTimeField(
        verbose_name="Timestamp", blank=True, null=True, editable=False
    )

    file_type = models.CharField(
        verbose_name="Filetype", blank=True, null=True, max_length=255
    )

    permalink = models.URLField(verbose_name="Permalink", blank=True, null=True)

    local_file = models.FileField(
        verbose_name="Local File", upload_to=multimedia_path, blank=True, null=True
    )

    is_file_downloaded = models.BooleanField(
        verbose_name="Is file downloaded?", default=False, db_index=True
    )

    def __str__(self):
        """
        String representation of model object.
        """

        return self.file_id if self.file_id is not None else ''

    @property
    def get_username(self):
        """
        Returns real user name.
        """

        return self.user.name

    @property
    def get_local_file(self):
        """
        Returns html form of downloadable file.
        """

        if self.local_file:
            _url = self.local_file.path.split("src")[-1]
            _src = settings.STATIC_ROOT.split("src")[-1]
            return mark_safe(
                f'<a href="{_url}" target="_blank"><img class="download" src="{_src}/template_static/icon/download.svg"></a>'
            )
        return mark_safe('<div class="loader"></div>')

    class Meta:
        verbose_name = "File"
        verbose_name_plural = "Files"
