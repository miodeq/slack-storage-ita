import re

from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import urlize as urlize_impl
from django.utils.safestring import mark_safe

from apps.core.models import User

register = template.Library()


@register.filter
def render_message(value):
    """
    Renders snippets and variable highlight in message.
    """
    render_filters = {"snippet": "```", "hl": "`", "mention": "<@(.*?)>"}
    _text = list()
    global_render = False

    def rendering(word, key):
        global global_render
        if str(word).startswith(render_filters.get(key)) and str(word) != render_filters.get(key):
            if str(word).endswith(render_filters.get(key)):
                start = word.index(render_filters.get(key)) + len(
                    render_filters.get(key)
                )
                end = word.index(render_filters.get(key), start)
                _text.append(f'<code class="{key}">{word[start:end]}</code>')
                global_render = False
            else:
                start = word.index(render_filters.get(key)) + len(
                    render_filters.get(key)
                )
                _text.append(f'<code class="{key}">{word[start:]}')
                global_render = True
        elif global_render:
            if str(word).endswith(render_filters.get(key)):
                end = word.index(render_filters.get(key))
                _text.append(f"{word[:end]}</code>")
                global_render = False
            else:
                _text.append(word)
                global_render = True
        else:
            end = word.index(render_filters.get(key))
            _text.append(f"{word[:end]}</code>")

    for word in value.split():
        slack_user = re.findall(render_filters.get("mention"), word)
        if render_filters.get("snippet") in word:
            rendering(word, "snippet")
        elif render_filters.get("hl") in word:
            rendering(word, "hl")
        elif slack_user:
            user = User.objects.filter(user_id=slack_user[0]).first()

            _text.append(f'<a class="mention" href="/users/{user.pk}"><b>@{user.name}</b></a>')
        else:
            _text.append(word)
            global_render = False

    return mark_safe(" ".join(_text))


@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def urlize_target_blank(value, autoescape=None):
    """
    Filter forces url to open in new tab.
    """
    return mark_safe(
        urlize_impl(value, nofollow=True, autoescape=autoescape).replace(
            "<a", '<a target="_blank"'
        )
    )
