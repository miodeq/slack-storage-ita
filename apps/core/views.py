# -*- coding: utf-8 -*-

import re

from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.db.models import Count
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.utils.safestring import mark_safe
from django.views import generic
from django.views.decorators.cache import cache_page

from apps.core.models import Channel, File, User, Message
from apps.core.models import TokenSingleton, Team
from apps.core.utils import init_logger

logger = init_logger(name=f"[{__name__}]")

CACHE_TTL = getattr(settings, "CACHE_TTL", DEFAULT_TIMEOUT)


# @method_decorator(cache_page(CACHE_TTL), name='dispatch')
class UserProfileView(generic.TemplateView):
    """
    User profile view.
    """

    template_name = "user_profile.html"

    def get_context_data(self, **kwargs):
        """
        Overridden context data.
        """

        context = super(UserProfileView, self).get_context_data(**kwargs)

        try:
            user = User.objects.get(pk=self.kwargs.get("pk"))
        except User.DoesNotExist:
            user = None

        context["channel_names"] = Channel.objects.values_list("name", flat=True)

        context["user"] = user

        context["token_exists"] = validate_token_exists()

        return context


def validate_token_exists():
    """
    Returns True if token has already been added, otherwise False.
    """

    try:
        TokenSingleton.objects.get()
        return True
    except TokenSingleton.DoesNotExist:
        return False


class SearchView(generic.ListView):
    """
    Search view.
    """

    template_name = "messages.html"
    model = Message
    context_object_name = "messages"
    paginate_by = 10

    @staticmethod
    def highlight(text, search):
        """
        Mark search result with color.
        """

        highlighted = re.sub(
            "(?i)(" + "|".join(map(re.escape, [search])) + ")",
            r"<mark><b>\1</b></mark>",
            text,
        )
        return mark_safe(highlighted)

    def get_search_query(self):
        """
        Returns search query from input.
        """

        return self.request.GET.get("search_query", [])

    def get_context_data(self, **kwargs):
        """
        Overridden context data.
        """

        context = super(SearchView, self).get_context_data()

        context["channel_names"] = Channel.objects.values_list("name", flat=True)
        context["search_query"] = self.get_search_query()
        context["is_search_results"] = True
        context["token_exists"] = validate_token_exists()

        return context

    def get_queryset(self):
        """
        Returns search result if search input used.
        """

        user_query = self.get_search_query()

        if user_query:
            qs = (
                self.model.objects.filter(msg__icontains=user_query)
                    .select_related("user")
                    .order_by("-pk")
            )
        else:
            qs = self.model.objects.none()

        for item in qs:
            item.msg = self.highlight(item.msg, user_query)

        return qs


class HomeView(generic.TemplateView):
    """
    Home view
    """

    template_name = "main.html"

    def post(self, request, *args, **kwargs):
        """
        Post method to retrieve token from form.
        """
        from apps.api.tasks import (
            collect_team_users,
            collect_messages_data,
            collect_files_data,
        )

        t = TokenSingleton.get_solo()
        t.token = self.request.POST.get("slack_token", [])
        t.task_active = True
        t.save()

        collect_team_users.delay()
        collect_messages_data.delay()
        collect_files_data.delay()

        return redirect("home")

    def get_context_data(self, **kwargs):
        """
        Overridden context data.
        """

        context = super(HomeView, self).get_context_data(**kwargs)

        context["channel_names"] = Channel.objects.values_list("name", flat=True)

        context["token_exists"] = validate_token_exists()

        try:
            context["team"] = Team.objects.last()
        except Team.DoesNotExist:
            context["team"] = False

        return context


@method_decorator(cache_page(CACHE_TTL), name="dispatch")
class FilesView(generic.ListView):
    """
    Files view.
    """

    model = File
    template_name = "files.html"
    context_object_name = "files"

    def get_context_data(self, **kwargs):
        """
        Overridden context dict to extend with additional data.
        """

        context = super(FilesView, self).get_context_data(**kwargs)

        context["channel_names"] = Channel.objects.values_list("name", flat=True)
        context["token_exists"] = validate_token_exists()

        return context

    def get_queryset(self):
        """
        Main queryset which will be visible when calling context_object_name variable.
        """

        return File.objects.select_related("user").only(
            "user__avatar", "user", "permalink", "file_type"
        ).order_by("-timestamp")


# @method_decorator(cache_page(CACHE_TTL), name='dispatch')
class UsersView(generic.ListView):
    """
    Users view.
    """

    model = User
    template_name = "users.html"
    context_object_name = "users"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        """
        Overridden context dict to extend with additional data.
        """

        context = super(UsersView, self).get_context_data(**kwargs)

        context["channel_names"] = Channel.objects.values_list("name", flat=True)
        context["token_exists"] = validate_token_exists()

        context["top_poster"] = User.objects.annotate(
            num_messages=Count("message")
        ).order_by("-num_messages").first()

        return context

    def get_queryset(self):
        """
        Main queryset which will be visible when calling context_object_name variable.
        """

        return User.objects.annotate(num_messages=Count("message")).order_by(
            "-num_messages"
        )


# @method_decorator(cache_page(CACHE_TTL), name='dispatch')
class MessagesView(generic.ListView):
    """
    Messages view.
    """

    model = Message
    template_name = "messages.html"
    context_object_name = "messages"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        """
        Overridden context dict to extend with additional data.
        """

        context = super(MessagesView, self).get_context_data(**kwargs)

        context["channel_names"] = Channel.objects.values_list("name", flat=True)
        context["channel_name"] = self.kwargs.get("channel_name")
        context["token_exists"] = validate_token_exists()

        return context

    def get_queryset(self):
        """
        Main queryset which will be visible when calling context_object_name variable.
        """

        return Message.objects.select_related("user").filter(
            channel__name=self.kwargs.get("channel_name")
        ).only("user__avatar").order_by("-pk")


def handler404(request, exception):
    """
    Error 404 view
    """
    context = {}

    return render(
        request=request, context=context, status=404, template_name="errors/404.html"
    )


def handler500(request):
    """
    Error 500 view
    """

    context = {}

    logger.error("status 500")

    return render(
        request=request, context=context, status=500, template_name="errors/500.html"
    )


def handler403(request, exception):
    """
    Error 403 view
    """

    context = {}

    return render(
        request=request, context=context, status=403, template_name="errors/403.html"
    )


def handler503(request):
    """
    Error 503 view
    """

    context = {}

    logger.error("status 503")

    return render(
        request=request, context=context, template_name="errors/503.html", status=503
    )
