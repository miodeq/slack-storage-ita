from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

admin.autodiscover()

urlpatterns = [path("admin/", admin.site.urls), path("", include("apps.core.urls"))]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = "apps.core.views.handler404"
handler403 = "apps.core.views.handler403"
handler500 = "apps.core.views.handler500"
handler503 = "apps.core.views.handler503"
