import os

import dj_database_url
from celery.schedules import crontab

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ["*"]

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_celery_beat",
    "django_extensions",
    "apps.core",
    "apps.api",
    "solo",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "slack_storage.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "slack_storage.wsgi.application"

DATABASE_URL = "postgres://{user}:{password}@postgres:5432/{db_name}".format(
    user=os.environ.get("DB_USER"),
    password=os.environ.get("DB_PASSWORD"),
    db_name=os.environ.get("DB_NAME"),
)

DATABASES = {"default": dj_database_url.config(default=DATABASE_URL, conn_max_age=600)}

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator", },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator", },
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator", },
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator", },
]

LANGUAGE_CODE = "en-us"

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

STATIC_URL = "/static/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")

CELERY_IMPORTS = ("apps.api.tasks",)

REDIS_URL = "redis://:{password}@{host}:6379/0".format(
    host=os.environ.get("REDIS_HOST"),
    password=os.environ.get("REDIS_PASSWORD")
)

CELERY_BROKER_URL = REDIS_URL

CELERY_RESULT_BACKEND = REDIS_URL
CELERY_SEND_EVENTS = True
CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
CELERY_ALWAYS_EAGER = False
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

CELERY_TIMEZONE = "Europe/Warsaw"

TIME_ZONE = CELERY_TIMEZONE

CELERY_BEAT_SCHEDULE = {}

CELERY_TASKS = {
    "collect_team_users": {
        "task": "collect_team_users",
        "schedule": crontab(minute="*/5"),
    },
    "collect_messages_data": {
        "task": "collect_messages_data",
        "schedule": crontab(minute="*/5"),
    },
    "collect_files_data": {
        "task": "collect_files_data",
        "schedule": crontab(minute="*/5"),
    },
    "collect_user_avatars": {
        "task": "collect_user_avatars",
        "schedule": crontab(hour="*/3"),
    },
    "collect_user_files": {
        "task": "collect_user_files",
        "schedule": crontab(minute="*/5"),
    },
}

CELERY_BEAT_SCHEDULE.update(CELERY_TASKS)

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_URL,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": os.environ.get("REDIS_PASSWORD"),
        },
    }
}

# 5 minutes
CACHE_TTL = 60 * 5

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

SESSION_CACHE_ALIAS = "default"
