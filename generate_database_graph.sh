#!/bin/bash

echo "${0}: [1] Generating database graph"
python manage.py graph_models core -o batabase_graph.png --settings=slack_storage.test_settings
