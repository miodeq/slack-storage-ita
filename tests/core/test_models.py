from datetime import datetime

import pytest
import pytz

from apps.core.models import Team, Channel, User, Message, File


class TestModels:
    """
    Test scenarios fore app "core" - models.
    """

    @pytest.mark.django_db
    def setup_method(self):
        self.team = Team.objects.create(
            team_id="0000001",
            name="sample_team",
            domain="domain.com",
            domain_email="admin@domain.com",
            icon=None,
        )

        self.user1 = User.objects.create(
            user_id="0000001",
            team=self.team,
            name="John Doe",
            avatar=None,
            phone="+123456789012",
            skype=None,
            is_admin=True,
            is_owner=False,
            is_bot=False,
        )

        self.user2 = User.objects.create(
            user_id="0000002",
            team=self.team,
            name="Jane Doe",
            avatar=None,
            phone="+1987654321012",
            skype="janedoe1212",
            is_admin=False,
            is_owner=False,
            is_bot=False,
        )

        self.channel = Channel.objects.create(
            channel_id="0000001",
            name="sample_channel",
            created_date=datetime.now(pytz.utc),
            creator=self.user1,
            topic="sample topic",
            purpose=None,
            num_members=2,
        )

        self.message1 = Message.objects.create(
            msg_id="0000001",
            channel=self.channel,
            user=self.user1,
            team=self.team,
            msg_type="message",
            msg="hello world",
            timestamp=datetime.now(pytz.utc),
        )

        self.message2 = Message.objects.create(
            msg_id="0000002",
            channel=self.channel,
            user=self.user2,
            team=self.team,
            msg_type="message",
            msg="hi",
            timestamp=datetime.now(pytz.utc),
        )

        self.message3 = Message.objects.create(
            msg_id="0000003",
            channel=self.channel,
            user=self.user1,
            team=self.team,
            msg_type="message",
            msg="test123",
            timestamp=datetime.now(pytz.utc),
        )

        self.file = File.objects.create(
            file_id="0000001",
            user=self.user1,
            channel=self.channel,
            timestamp=datetime.now(pytz.utc),
            file_type="application/pdf",
            permalink="http://test.com/file.pdf",
        )

    @pytest.mark.django_db
    def teardown_method(self):
        self.team.delete()

    @pytest.mark.django_db
    def test_create_team(self):
        """
        Test Case 1.1 - Creating team object.
        """

        team_name = "test_team"

        created_team = Team.objects.create(team_id="t000000", name=team_name)
        queried_team = Team.objects.get(team_id="t000000")

        assert created_team == queried_team
        assert queried_team.domain == None
        assert queried_team.domain_email == None
        assert queried_team.icon == None

    @pytest.mark.django_db
    def test_create_user(self):
        """
        Test Case 1.2 - Creating user object.
        """

        user_name = "test_user"

        created_user = User.objects.create(user_id="t000000", name=user_name)

        queried_user = User.objects.get(user_id="t000000")

        assert created_user == queried_user

        assert queried_user.team == None
        assert queried_user.avatar == None
        assert queried_user.phone == None
        assert queried_user.skype == None
        assert queried_user.is_admin == False
        assert queried_user.is_owner == False
        assert queried_user.is_bot == False

    @pytest.mark.django_db
    def test_create_channel(self):
        """
        Test Case 1.3 - Creating channel object.
        """

        channel_name = "test_channel"

        created_channel = Channel.objects.create(
            channel_id="t000000", name=channel_name
        )

        queried_channel = Channel.objects.get(channel_id="t000000")

        assert created_channel == queried_channel

        assert queried_channel.created_date == None
        assert queried_channel.creator == None
        assert queried_channel.topic == None
        assert queried_channel.purpose == None
        assert queried_channel.num_members == 0

    @pytest.mark.django_db
    def test_create_message(self):
        """
        Test Case 1.4 - Creating message object.
        """

        created_message = Message.objects.create(msg_id="t000000", msg="test_message")

        queried_message = Message.objects.get(msg_id="t000000")

        assert created_message == queried_message

        assert queried_message.channel == None
        assert queried_message.user == None
        assert queried_message.team == None
        assert queried_message.msg_type == None
        assert queried_message.timestamp == None

    @pytest.mark.django_db
    def test_create_file(self):
        """
        Test Case 1.5 - Creating file object.
        """

        created_file = File.objects.create(
            file_id="t000000", file_type="application/test"
        )

        queried_file = File.objects.get(file_id="t000000")

        assert created_file == queried_file

        assert queried_file.user == None
        assert queried_file.channel == None
        assert queried_file.timestamp == None
        assert queried_file.permalink == None
        assert queried_file.local_file == ""

    @pytest.mark.django_db
    def test_check_user_relation(self):
        """
        Test Case 1.6 - Checking user relation with team.
        """

        queried_user = User.objects.get(user_id=self.user1.user_id)

        assert queried_user.team == self.team

    @pytest.mark.django_db
    def test_check_channel_relation(self):
        """
        Test Case 1.7 - Checking channel relation with creator.
        """

        queried_channel = Channel.objects.get(channel_id=self.channel.channel_id)

        assert queried_channel.creator == self.user1

    @pytest.mark.django_db
    def test_check_message_relation(self):
        """
        Test Case 1.8 - Checking message relations.
        """

        queried_message = Message.objects.get(msg_id=self.message1.msg_id)

        assert queried_message.channel == self.channel
        assert queried_message.user == self.user1
        assert queried_message.team == self.team

    @pytest.mark.django_db
    def test_file_relation(self):
        """
        Test Case 1.9 - Checking file relation to user.
        """

        queried_file = File.objects.get(file_id=self.file.file_id)

        assert queried_file.user == self.user1
        assert queried_file.channel == self.channel

    @pytest.mark.django_db
    def test_check_team_str_method(self):
        """
        Test Case 1.10 - Checking Team model object __str__ method.
        """

        assert self.team.__str__() == self.team.name

    @pytest.mark.django_db
    def test_check_user_str_method(self):
        """
        Test Case 1.11 - Checking User model object __str__ method.
        """

        assert self.user1.__str__() == self.user1.name

    @pytest.mark.django_db
    def test_check_channel_str_method(self):
        """
        Test Case 1.12 - Checking Channel model object __str__ method.
        """

        assert self.channel.__str__() == self.channel.name

    @pytest.mark.django_db
    def test_check_message_property_method(self):
        """
        Test Case 1.13 - Checking Message model object @property method.
        """

        assert self.message1.get_msg == self.message1.msg

    @pytest.mark.django_db
    def test_check_message_str_method(self):
        """
        Test Case 1.14 - Checking Message model object __str__ method.
        """

        assert self.message1.__str__() == self.message1.msg_id

    @pytest.mark.django_db
    def test_check_message_repr_method(self):
        """
        Test Case 1.15 - Checking Message model object __repr__ method.
        """

        assert (
                self.message1.__repr__()
                == f"<{self.message1.channel.name}>: <{self.message1.msg}>"
        )

    @pytest.mark.django_db
    def test_check_file_int_method(self):
        """
        Test Case 1.16 - Checking File model object __int__ method.
        """

        assert self.file.__int__() == self.file.id

    @pytest.mark.django_db
    def test_check_file_str_method(self):
        """
        Test Case 1.17 - Checking File model object __str__ method.
        """

        assert self.file.__str__() == self.file.file_id
