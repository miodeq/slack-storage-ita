import os
import time


class TestUtils:
    """
    Test scenarios for app "core" - utils package.
    """

    def test_prepare_directories(self):
        """
        Test Case 1.1 - Verifying dirs creation for upload media.
        """

        from django.conf import settings
        from apps.core.utils import prepare_directories

        name = "test"

        data_dirs_datetime = str(time.strftime("%y/%m/%d/%H/%M"))
        file_path = os.path.join(f"{settings.MEDIA_ROOT}/{name}/{data_dirs_datetime}")

        prepare_directories(name=name)

        assert os.path.exists(file_path) == True

    def test_multimedia_path(self):
        """
        Test Case 1.2 - Verifying created file path for multimedia path.
        """

        from apps.core.utils import multimedia_path

        filename = "test_file.png"

        path = multimedia_path(None, filename)
        _, ext = os.path.splitext(path)

        assert filename not in path
        assert ext == ".png"
