from datetime import datetime

import pytz

from apps.api.utils import convert_message_timestamp


class TestUtils:
    """
    Test scenarios for utils package.
    """

    def test_convert_msg_timestamp(self):
        """
        Test Case 1.1 - Verifying  UTC timestamp conversion into datetime object.
        """

        tz = pytz.timezone("UTC")
        original_timestamp = 1199145600

        original_datetime = tz.localize(datetime.utcfromtimestamp(original_timestamp))

        converted_datetime = convert_message_timestamp(original_timestamp)

        assert original_datetime == converted_datetime
