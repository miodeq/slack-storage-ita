from django.test import override_settings, TestCase

from apps.api.tasks import (
    collect_team_users,
    collect_messages_data,
    collect_user_avatars,
    collect_files_data,
    collect_user_files,
)


class TestTasks(TestCase):
    """
    Test scenarios for tasks package.
    """

    # @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    # def test_collect_team_users(self):
    #     """
    #     Test Case 1.1 - Collecting team and user info.
    #     """

    #     self.assertTrue(collect_team_users.delay())

    # @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    # def test_collect_user_avatars(self):
    #     """
    #     Test Case 1.2 - Collecting user avatars task.
    #     """

    #     self.assertTrue(collect_user_avatars.delay())

    # @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    # def test_collect_messages_data(self):
    #     """
    #     Test Case 1.3 - Collecting user messages task.
    #     """

    #     self.assertTrue(collect_messages_data.delay())

    # @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    # def test_collect_files_data(self):
    #     """
    #     Test Case 1.4 - Collecting user files info task.
    #     """

    #     self.assertTrue(collect_files_data.delay())

    # @override_settings(CELERY_TASK_ALWAYS_EAGER=True)
    # def test_collect_user_files(self):
    #     """
    #     Test Case 1.5 - Collecting user files task.
    #     """

    #     self.assertTrue(collect_user_files.delay())
