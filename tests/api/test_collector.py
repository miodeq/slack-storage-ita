import os

from django.test import TestCase

from apps.api.collector import SlackCollector


class TestCollector(TestCase):
    """
    Test scenarios for tasks package.
    """

    def setUp(self):
        self.collector = SlackCollector(token=os.environ.get("TEST_TOKEN"))
        self.collector.run()

    def test_collector_connection(self):
        """
        Test Case 1.1 - Verifying connection with Slack API.
        """

        assert self.collector.client.api_test()["ok"]

    def test_get_get_team_data(self):
        """
        Test Case 1.2 - Verifying collector method - get_team_data().
        """

        team = self.collector.get_team_data()

        assert "team_id" in team.keys() and len(team["team_id"]) > 0

        assert "name" in team.keys() and len(team["name"]) > 0

    def test_get_channels_data(self):
        """
        Test Case 1.3 - Verifying collector method - get_channels_data().
        """

        assert len(self.collector.get_channels_data()) > 0

    def test_get_users_data(self):
        """
        Test Case 1.4 - Verifying collector method - get_users_data().
        """

        assert len(self.collector.get_users_data()) > 0

    def test_get_messages_info(self):
        """
        Test Case 1.5 - Verifying collector method - get_messages_data().
        """

        assert len(self.collector.get_messages_data()) > 0

    def test_get_files_data(self):
        """
        Test Case 1.6 - Verifying collector method - get_files_data().
        """

        assert len(self.collector.get_files_data()) > 0
