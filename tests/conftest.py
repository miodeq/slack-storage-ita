import os

import django
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slack_storage.settings")


def pytest_configure():
    settings.DEBUG = True

    django.setup()
