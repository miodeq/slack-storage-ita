#!/bin/sh

NAME="slack_storage"
SERVER="0.0.0.0:8000"
NUM_WORKERS=1
NUM_THREADS=3

set -e

#echo "${0}: running code analysis with pylint..."
#./pylint.sh

echo "${0}: running migrations."
python manage.py migrate

#echo "${0}: running tests."
#python -m pytest -s -v --cov-report=html --cov=. tests/

echo "${0}: collecting statics."
python manage.py collectstatic --noinput

echo "${0}: creating admin user."
echo "from django.contrib.auth.models import User; print(\"Admin exists\") if User.objects.filter(username='$DJANGO_USER').exists() else User.objects.create_superuser('$DJANGO_USER', '$DJANGO_MAIL', '$DJANGO_PASSWORD')" | python manage.py shell

echo "${0}: running app."

gunicorn slack_storage.wsgi:application \
	--name=$NAME \
	--bind=$SERVER \
	--timeout=900 \
	--workers=$NUM_WORKERS \
	--threads=$NUM_THREADS \
	--worker-connections=1000 \
	--log-level=info \
	--reload \
	--reload-engine inotify

exec "$@"
