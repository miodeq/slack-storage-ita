#!/bin/sh

sleep 5
celery -A slack_storage.celery worker -l INFO -Q slack_api_collector -n slack_api_collector@%h -Ofair --autoscale=4,4 --maxtasksperchild=4