aiohttp==3.6.2
amqp==2.5.2
appnope==0.1.0
astroid==2.3.3
async-timeout==3.0.1
attrs==19.3.0
Babel==2.7.0
backcall==0.1.0
billiard==3.6.1.0
celery==4.3.0
certifi==2019.9.11
chardet==3.0.4
coverage==5.0
decorator==4.4.1
dj-database-url==0.5.0
Django==2.2.7
django-celery-beat==1.5.0
django-extensions==2.2.5
django-redis==4.10.0
django-solo==1.1.3
django-timezone-field==3.1
flower==0.9.3
gunicorn==20.0.4
idna==2.8
importlib-metadata==1.1.0
inotify==0.2.10
ipython==7.10.1
ipython-genutils==0.2.0
isort==4.3.21
jedi==0.15.1
kombu==4.6.6
lazy-object-proxy==1.4.3
mccabe==0.6.1
more-itertools==8.0.0
multidict==4.6.1
nose==1.3.7
packaging==19.2
parso==0.5.1
pexpect==4.7.0
pickleshare==0.7.5
pluggy==0.13.1
pre-commit==1.21.0
prompt-toolkit==3.0.2
psycopg2==2.8.4
ptyprocess==0.6.0
py==1.8.0
pydot==1.4.1
Pygments==2.5.2
pylint==2.4.4
pylint-django==2.0.13
pylint-plugin-utils==0.6
pyparsing==2.4.5
pytest==5.3.2
pytest-cov==2.8.1
pytest-django==3.7.0
python-crontab==2.4.0
python-dateutil==2.8.1
pytz==2019.3
redis==3.3.11
requests==2.22.0
six==1.13.0
slackclient==2.3.1
sqlparse==0.3.0
tornado==5.1.1
traitlets==4.3.3
typed-ast==1.4.0
urllib3==1.25.7
user-agent==0.1.9
vine==1.3.0
wcwidth==0.1.7
wrapt==1.11.2
yarl==1.3.0
zipp==0.6.0
