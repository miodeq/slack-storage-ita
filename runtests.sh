#!/bin/sh

python -m pytest --ds=slack_storage.test_settings -s -v --cov-report=html --cov=. tests/
