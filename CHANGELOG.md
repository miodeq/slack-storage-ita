### Version v1.0.0

**Release date: 13.12.2019**

- Added working cache for messages;
- Added packages for django-toolbar to requirements;

**Release date: 10.12.2019**

- Extended functional views into CBV;

**Release date: 30.11.2019**

- Added handling errors: 403, 404, 500, 503;
- Added templates for specific errors, modified `urls.py` and `views.py` at `core` app;
- Added script `cleaner.sh` for cleaning application's cache;

**Release date: 29.11.2019**

- Added path to channel to show channel messages
- Update `views.py`:
  -Create menagment with database to send informations to `files.html`
  -Create menagment with database to send informations to `users.html`
  -Create menagment with database to send informations to `messages.html`
- Create list of channels based on sql query
- Edited templates to show context:
  - Modified `files.html`, `users.html`, `messages.html`

**Release date: 26.11.2019**

- Added path for files upload;
- Made cleanup with models.py and admin.py at `core` app;
- Adjusted templates to be rendered in `views.py` at `core` app;
- Added script for running application;

**Release date: 25.11.2019**

- Add collector to repository;
- Removed unused imports;
- Cleaned main docstring;
- Removed staticmethod for changing timestamp to utc;
- Removed change to utc ussage on timestamps;
- Wrote docstrings;
- Removed timestamp in get_messages method. ts is more usefull to work with;
- Removed prints;
- Rewrite "write json to file" to return object in memory insted of writing file;

**Release date: 18.11.2019**

- Initial commit;
- Created README.md, CHANGELOG.md files;
- Created `python env` requirements.txt file;
- Created .gitignore;

**Release date: 25.11.2019**

- Added app 'core' in slack/settings.py;
- Created models User, Channel, File, Team, Message in core/models.py;
- Created admin view for all models in core/admin.py;
