SlackStorage
============

**Current stable version:** v1.0.0

**Release date:** 21.02.2020

### Authors:
Maciej Brzostowski <wohnyjalker@gmail.com>;

Maciej Ferenc <ferenc312@gmail.com>;

Adrian Sitko <kayen95@gmail.com>;

Magda Bielawska <magdabielawska92@gmail.com>;

Dawid Bińczyk <dawid.binczyk@protonmail.com>;

Maciej Wróblewski <toniejatomojashauma@gmail.com>;

Przemysław Baj <przemekbaj1@gmail.com>;

Andrzej Karawaj <andrzejkar17@gmail.com>;

Maciej Popławski <slynnyfotograf@gmail.com>;

### Development environment preparation (in virtualenv):
```
pip install -r requirements.txt
pre-commit install
```

### Create .env file with required config:
```bash
SECRET_KEY=xxx

DB_USER=xxx
DB_PASSWORD=xxx
DB_NAME=xxx

DJANGO_USER=xxx
DJANGO_MAIL=xxx
DJANGO_PASSWORD=xxx

REDIS_HOST=xxx
REDIS_PASSWORD=xxx
```

### Building and running docker:
```
./rundocker.sh
```

### Nginx static files error 403 workaround

Run dockerand then go to root folder of project
```
sudo chown -R user_name static/
sudo chmod -R o+rx static
```
Kill docker and run again

## Dumping db (once celery task fetched data):
```bash
docker ps | grep web

docker exec -it it_academy_web_1 bash

./generate_dbdump.sh
```
then db dump file is available at:
```bash
localhost/media/slack_db.json.gz

wget localhost/media/slack_db.json.gz
```

## Loading dumped db:
```bash
docker cp slack_db.json it_academy_web_1:/src

docker exec -it it_academy_web_1 bash

python manage.py loaddata slack_db.json
```

## Available pre-commit hooks:
https://pre-commit.com/hooks.html
